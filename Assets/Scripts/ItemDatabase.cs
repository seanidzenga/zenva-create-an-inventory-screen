﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour {
    
    public List<Item> items;

    void Awake() {

        BuildDatabase();
    }

    public Item GetItem(int id) {

        return items.Find( item => item.id == id );
    }

    public Item GetItem(string name ) {

        return items.Find( item => item.title == name );
    }

    void BuildDatabase() {

        items = new List<Item>() {
            new Item(
                    0,
                    "Diamond Sword",
                    "A Sword made of diamonds!",
                    new Dictionary<string, int>() {
                        { "Power", 15 },
                        { "Defence", 10 }
                    }
                ),
            new Item(
                    1,
                    "Diamond Ore",
                    "A shiny diamond",
                    new Dictionary<string, int>() {
                        { "Value", 300 }
                    }
                ),
            new Item(
                    2,
                    "Diamond Pick",
                    "A Pick made of diamonds!",
                    new Dictionary<string, int>() {
                        { "Power", 5 },
                        { "Defence", 0 },
                        { "Durability", 16 }
                    }
                ),
            new Item(
                    3,
                    "Emerald Ore",
                    "A beautiful emerald",
                    new Dictionary<string, int>() {
                        { "Value", 80 }
                    }
                ),
            new Item(
                    4,
                    "Gold Ore",
                    "A nugget of gold!",
                    new Dictionary<string, int>() {
                        { "Value", 200 }
                    }
                ),
            new Item(
                    5,
                    "Silver Pick",
                    "Why does this exist?!",
                    new Dictionary<string, int>() {
                        { "Power", 1 },
                        { "Defence", 0 },
                        { "Durability", 2 }
                    }
                )
        };
    }

}
