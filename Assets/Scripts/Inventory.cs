﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public List<Item> playerItems = new List<Item>();
    public ItemDatabase itemDatabase;
    public UIInventory inventoryUI;

    private void Start() {

        AddItem( 0 );
        AddItem( 1 );
        AddItem( 2 );
        AddItem( 3 );
        AddItem( 4 );
        AddItem( 5 );
    }

    private void Update() {

        if( Input.GetKeyDown( KeyCode.I ) ) {

            inventoryUI.gameObject.SetActive( !inventoryUI.gameObject.activeSelf );
        }
    }

    public void AddItem(int id) {

        Item itemToAdd = itemDatabase.GetItem( id );

        if(itemToAdd != null ) {
            playerItems.Add( itemToAdd );
            inventoryUI.AddNewItem( itemToAdd );
            Debug.Log( "Added item: " + itemToAdd.title );
        }
    }

    public void AddItem( string name ) {

        Item itemToAdd = itemDatabase.GetItem( name );

        if(itemToAdd != null ) {
            playerItems.Add( itemToAdd );
            inventoryUI.AddNewItem( itemToAdd );
            Debug.Log( "Added item: " + itemToAdd.title );
        }
    }

    public Item CheckForItem(int id) {

        return playerItems.Find( item => item.id == id );
    }

    public void RemoveItem(int id ) {

        Item itemToRemove = CheckForItem( id );

        if(itemToRemove != null ) {

            playerItems.Remove( itemToRemove );
            inventoryUI.RemoveItem( itemToRemove );
        }
    }

}
